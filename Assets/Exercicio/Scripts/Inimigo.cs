﻿using UnityEngine;
using System.Collections;

public class Inimigo : MonoBehaviour
{
    // OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bala")
        {
            Destroy(gameObject);
        }
    }
}
