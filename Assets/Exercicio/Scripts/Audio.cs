﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Audio : MonoBehaviour {

    public AudioClip AberturaGame;
    AudioSource audioClip;
	void Start () {
        audioClip = GetComponent<AudioSource>();
        audioClip.PlayOneShot(AberturaGame, 0.7f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
