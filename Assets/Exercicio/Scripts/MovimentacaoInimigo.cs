﻿using UnityEngine;
using System.Collections;

public class MovimentacaoInimigo : MonoBehaviour {
    public Transform player;
    // Update is called once per frame
    void Update()
    {
        distanciaInimigo();

    }

    private void distanciaInimigo()
    {
        float distanciaJogador = Vector3.Distance(transform.position, player.position);

        if (distanciaJogador < 10 && distanciaJogador > 1)
        {
            ControlarInimigo(true);
        }
        else
        {
            ControlarInimigo(false);
        }
    }

    private void ControlarInimigo(bool v)
    {
        if (v)
        {
            Vector3 Direcao = (player.position - transform.position).normalized;
            transform.position += Direcao * Time.deltaTime * 5;
            Quaternion newRotation = Quaternion.LookRotation(Direcao);
            transform.rotation = newRotation;

        }
        else
        {
            return;
        }
    }
}