﻿#pragma strict
var velocidadeFrente	:	float;
var velocidadeCima		:	float;
var velocidadeLado		:	float;
var velocidadeRotacao	:	float;
function Start () {
	velocidadeCima		=	3 * Time.deltaTime;
	velocidadeFrente	=	5 * Time.deltaTime;
	velocidadeLado		=	4 * Time.deltaTime;
	velocidadeRotacao	=	30 * Time.deltaTime;
}

function Update () {
	if(Input.GetKey("w")){
	transform.Translate(0,0,velocidadeFrente);
	}
	if(Input.GetKey("s")){
	transform.Translate(0,0,-velocidadeFrente);
	}
	if(Input.GetKey("a")){
	transform.Translate(-velocidadeLado,0,0);
	}
	if(Input.GetKey("d")){
	transform.Translate(velocidadeLado,0,0);
	}
	if(Input.GetKey("j")){
	transform.Rotate(0,velocidadeRotacao,0);
	}
	if(Input.GetKey("l")){
	transform.Rotate(0,-velocidadeRotacao,0);
	}


}