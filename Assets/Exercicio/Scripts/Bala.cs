﻿using UnityEngine;
using System.Collections;
public class Bala : MonoBehaviour
{

    public float velocidadeBala;

    // Update is called once per frame
    void Update()
    {
        velocidadeBala = 6 * Time.deltaTime;
        transform.Translate(velocidadeBala, 0, 0);
    }

    // OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider
    public void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
