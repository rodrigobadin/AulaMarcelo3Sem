﻿using UnityEngine;
using System.Collections;

public class MovimentoSetas : MonoBehaviour {
	float 	velocidadeFrente,
			velocidadeCima,
			velocidadeLado,
			velocidadeRotacao;

	// Use this for initialization
	void Start () {
		velocidadeCima		=	100 * Time.deltaTime;
		velocidadeFrente	=	5 * Time.deltaTime;
		velocidadeLado		=	4 * Time.deltaTime;
		velocidadeRotacao	=	30 * Time.deltaTime;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey("w")|Input.GetKey("up")){
			transform.Translate(0,0,velocidadeFrente);
		}
		if(Input.GetKey("s") | Input.GetKey("down"))
        {
			transform.Translate(0,0,-velocidadeFrente);
		}
		if(Input.GetKey("a") | Input.GetKey("left"))
        {
			transform.Translate(-velocidadeLado,0,0);
		}
		if(Input.GetKey("d") | Input.GetKey("right"))
        {
			transform.Translate(velocidadeLado,0,0);
		}
        if (Input.GetKeyDown("space"))
        {
            transform.Translate(0, velocidadeCima, 0);
        }
        if (Input.GetKey("j")){
			transform.Rotate(0,velocidadeRotacao,0);
		}
		if(Input.GetKey("l")){
			transform.Rotate(0,-velocidadeRotacao,0);
		}
	}
}
