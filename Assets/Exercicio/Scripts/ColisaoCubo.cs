﻿using UnityEngine;
using System.Collections;

public class ColisaoCubo : MonoBehaviour
{
    public GameObject myCollision;

    // OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider
    public void OnCollisionEnter(Collision collision)
    {
        if(myCollision.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
        }
    }
}
