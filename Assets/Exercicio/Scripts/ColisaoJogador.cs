﻿using UnityEngine;
using System.Collections;

public class ColisaoJogador : MonoBehaviour
{
    public GameObject myCollision;

    // OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider
    public void OnCollisionEnter(Collision collision)
    {
        if (myCollision.gameObject.tag == "Player")
        {
            print("OnCollisionEnter");
            myCollision.GetComponent<Rigidbody>().AddForce(1000, 1000, 0);
        }

    }
    public void OnCollisionStay(Collision collision)
    {
        if (myCollision.gameObject.tag == "Player")
        {
            print("OnCollisionStay");
        }

    }
    public void OnCollisionExit(Collision collision)
    {
        if (myCollision.gameObject.tag == "Player")
        {
            print("OnCollisionExit");
        }

    }
}
