﻿using UnityEngine;
using System.Collections;

public class CarregaArma : MonoBehaviour {
    public GameObject myCollision;
    public Arma arma;
    // OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider
    public void OnCollisionEnter(Collision collision)
    {
        if (myCollision.gameObject.tag == "Player")
        {
            arma.contMunicao += 10;
            arma.guns.text = "BALAS: " + arma.contMunicao.ToString();

        }
    }

}
