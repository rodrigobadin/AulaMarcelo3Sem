﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public bool mostrarGUI = true;
    private Rect janelaGUI = new Rect(10, 10, 120, 270);

    // Use this for initialization
    void Start()
    {
        OnGUI();
    }

    // Update is called once per frame
    void Update()
    {

    }

    // OnGUI is called for rendering and handling GUI events
    void OnGUI()
    {
 
        if (mostrarGUI)
        {
            janelaGUI = GUI.Window(0, janelaGUI, janelaConteudo, "Menu");
        }
    }
   public static void janelaConteudo(int id)
    {
        if (GUI.Button(new Rect(10, 30, 100, 20), "Exercicio 1"))
        {
            SceneManager.LoadScene(1);
        }
        if (GUI.Button(new Rect(10, 60, 100, 20), "Exercicio 2"))
        {
            SceneManager.LoadScene(2);
        }
        if (GUI.Button(new Rect(10, 90, 100, 20), "Exercicio 3"))
        {
            SceneManager.LoadScene(3);
        }
        if (GUI.Button(new Rect(10, 120, 100, 20), "Exercicio 4"))
        {
            SceneManager.LoadScene(4);
        }
        if (GUI.Button(new Rect(10, 150, 100, 20), "Exercicio 5"))
        {
            SceneManager.LoadScene(5);
        }

        if (GUI.Button(new Rect(10, 180, 100, 20), "Exercicio 6"))
        {
            SceneManager.LoadScene(6);
        }
        if (GUI.Button(new Rect(10, 210, 100, 20), "Exercicio 7"))
        {
            SceneManager.LoadScene(7);
        }
        if (GUI.Button(new Rect(10, 240, 100, 20), "Exercicio 8"))
        {
            SceneManager.LoadScene(8);
        }
    }


}
